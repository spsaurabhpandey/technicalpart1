# technicalPart1

<br>This project has some integration tests for a react app located at https://bitbucket.org/otrium/basic-contact-app/src/main/
<br>The integration tests have been created by extending cypress with cypress testing library plugin.

# How to run the frontend?
<br>Move inside app folder and run below command 
<br>### npm install
<br>### `npm start`
<br>Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## How to run server?  
<br>Move inside server folder and run below command 
<br>### npm install
<br>### `npm start`

# Where can we see the integration tests for the app?
<br>At path : technicalpart1\app\cypress\integration\testPart1.js

# How can we run the cypress tests?
 <br> 1. Ensure that the app and the server are running.
 <br> 2. Move inside app folder and run below command
 <br> 'npm run cy:test'  or 'npx cypress open'
 <br> 3. select a browser and run the testPart1.js


# Getting Started with Create React App and learn

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

