/* eslint-disable no-undef */
describe('tests for technical assigment1', () => {

    beforeEach('app renders correctly', () => {
        cy.visit('/');
    });

    it('Check the interface link between Search Contacts input element and contact list', () => {
        cy.findByPlaceholderText('Search Contacts').type('sss');
        cy.get('div[class="ui celled list"]').within(() => {
            cy.contains('sss').should('exist');
            cy.contains('sss').should('be.visible');
        });

    });

    it('Check the interface link between Add Contact button and Contact List', () => {
        cy.findByText('Add Contact').click();
        cy.findByPlaceholderText('Name').type('John Doe');
        cy.findByPlaceholderText('Email').type('johndoe@email.com');
        cy.findByText('Add').click();
        cy.get('div[class="ui celled list"]').within(() => {
            cy.contains('John Doe').should('exist');
            cy.contains('John Doe').should('be.visible');
            cy.contains('johndoe@email.com').should('exist');
            cy.contains('johndoe@email.com').should('be.visible');
        });

    })
})